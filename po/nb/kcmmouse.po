# Translation of kcmmouse to Norwegian Bokmål
#
# Hans Petter Bieker <bieker@kde.org>, 1998, 1999, 2000.
# Knut Yrvin <knut.yrvin@gmail.com>, 2002, 2005.
# Knut Erik Hollund <khollund@responze.net>, 2003.
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2003, 2007, 2008, 2010, 2011, 2014, 2015.
# Axel Bojer <fri_programvare@bojer.no>, 2005, 2006.
# Nils Kristian Tomren <slx@nilsk.net>, 2005, 2007.
# Bjørn Kvisli <bjorn.kvisli@gmail.com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: kcminput\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:38+0000\n"
"PO-Revision-Date: 2015-04-26 21:24+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Axel Bojer"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "fri_programvare@bojer.no"

#: backends/kwin_wl/kwin_wl_backend.cpp:66
#, kde-format
msgid "Querying input devices failed. Please reopen this settings module."
msgstr ""

#: backends/kwin_wl/kwin_wl_backend.cpp:86
#, kde-format
msgid "Critical error on reading fundamental device infos of %1."
msgstr ""

#: kcm/libinput/libinput_config.cpp:100
#, kde-format
msgid ""
"Error while loading values. See logs for more information. Please restart "
"this configuration module."
msgstr ""

#: kcm/libinput/libinput_config.cpp:105
#, kde-format
msgid "No pointer device found. Connect now."
msgstr ""

#: kcm/libinput/libinput_config.cpp:116
#, kde-format
msgid ""
"Not able to save all changes. See logs for more information. Please restart "
"this configuration module and try again."
msgstr ""

#: kcm/libinput/libinput_config.cpp:136
#, kde-format
msgid ""
"Error while loading default values. Failed to set some options to their "
"default values."
msgstr ""

#: kcm/libinput/libinput_config.cpp:158
#, kde-format
msgid ""
"Error while adding newly connected device. Please reconnect it and restart "
"this configuration module."
msgstr ""

#: kcm/libinput/libinput_config.cpp:182
#, kde-format
msgid "Pointer device disconnected. Closed its setting dialog."
msgstr ""

#: kcm/libinput/libinput_config.cpp:184
#, kde-format
msgid "Pointer device disconnected. No other devices found."
msgstr ""

#: kcm/libinput/main.qml:74
#, kde-format
msgid "Device:"
msgstr "Enhet:"

#: kcm/libinput/main.qml:98 kcm/libinput/main_deviceless.qml:50
#, kde-format
msgid "General:"
msgstr ""

#: kcm/libinput/main.qml:99
#, kde-format
msgid "Device enabled"
msgstr ""

#: kcm/libinput/main.qml:119
#, kde-format
msgid "Accept input through this device."
msgstr ""

#: kcm/libinput/main.qml:124 kcm/libinput/main_deviceless.qml:51
#, kde-format
msgid "Left handed mode"
msgstr ""

#: kcm/libinput/main.qml:144 kcm/libinput/main_deviceless.qml:71
#, kde-format
msgid "Swap left and right buttons."
msgstr ""

#: kcm/libinput/main.qml:149 kcm/libinput/main_deviceless.qml:76
#, kde-format
msgid "Press left and right buttons for middle-click"
msgstr ""

#: kcm/libinput/main.qml:169 kcm/libinput/main_deviceless.qml:96
#, kde-format
msgid ""
"Clicking left and right button simultaneously sends middle button click."
msgstr ""

#: kcm/libinput/main.qml:178 kcm/libinput/main_deviceless.qml:106
#, kde-format
msgid "Pointer speed:"
msgstr ""

#: kcm/libinput/main.qml:277 kcm/libinput/main_deviceless.qml:138
#, kde-format
msgid "Pointer acceleration:"
msgstr "Pekerakselerasjon:"

#: kcm/libinput/main.qml:308 kcm/libinput/main_deviceless.qml:169
#, kde-format
msgid "None"
msgstr ""

#: kcm/libinput/main.qml:312 kcm/libinput/main_deviceless.qml:173
#, kde-format
msgid "Cursor moves the same distance as the mouse movement."
msgstr ""

#: kcm/libinput/main.qml:318 kcm/libinput/main_deviceless.qml:179
#, kde-format
msgid "Standard"
msgstr ""

#: kcm/libinput/main.qml:322 kcm/libinput/main_deviceless.qml:183
#, kde-format
msgid "Cursor travel distance depends on the mouse movement speed."
msgstr ""

#: kcm/libinput/main.qml:334 kcm/libinput/main_deviceless.qml:195
#, kde-format
msgid "Scrolling:"
msgstr ""

#: kcm/libinput/main.qml:335 kcm/libinput/main_deviceless.qml:196
#, kde-format
msgid "Invert scroll direction"
msgstr ""

#: kcm/libinput/main.qml:351 kcm/libinput/main_deviceless.qml:212
#, kde-format
msgid "Touchscreen like scrolling."
msgstr ""

#: kcm/libinput/main.qml:356
#, kde-format
msgid "Scrolling speed:"
msgstr ""

#: kcm/libinput/main.qml:406
#, kde-format
msgctxt "Slower Scroll"
msgid "Slower"
msgstr ""

#: kcm/libinput/main.qml:413
#, kde-format
msgctxt "Faster Scroll Speed"
msgid "Faster"
msgstr ""

#: kcm/libinput/main.qml:424
#, kde-format
msgctxt "@action:button"
msgid "Re-bind Additional Mouse Buttons…"
msgstr ""

#: kcm/libinput/main.qml:462
#, kde-format
msgctxt "@label for assigning an action to a numbered button"
msgid "Extra Button %1:"
msgstr ""

#: kcm/libinput/main.qml:492
#, kde-format
msgctxt "@action:button"
msgid "Press the mouse button for which you want to add a key binding"
msgstr ""

#: kcm/libinput/main.qml:493
#, kde-format
msgctxt "@action:button, %1 is the translation of 'Extra Button %1' from above"
msgid "Enter the new key combination for %1"
msgstr ""

#: kcm/libinput/main.qml:497
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr ""

#: kcm/libinput/main.qml:514
#, kde-format
msgctxt "@action:button"
msgid "Press a mouse button "
msgstr ""

#: kcm/libinput/main.qml:515
#, kde-format
msgctxt "@action:button, Bind a mousebutton to keyboard key(s)"
msgid "Add Binding…"
msgstr ""

#: kcm/libinput/main.qml:544
#, kde-format
msgctxt "@action:button"
msgid "Go back"
msgstr ""
