# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# SPDX-FileCopyrightText: 2023 Enol P. <enolp@softastur.org>
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-24 00:38+0000\n"
"PO-Revision-Date: 2023-12-14 23:00+0100\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: Assamese <alministradores@softastur.org>\n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.4\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Xeneral"

#: package/contents/ui/code/tools.js:32
#, kde-format
msgid "Remove from Favorites"
msgstr ""

#: package/contents/ui/code/tools.js:36
#, kde-format
msgid "Add to Favorites"
msgstr ""

#: package/contents/ui/code/tools.js:60
#, kde-format
msgid "On All Activities"
msgstr "En toles actividaes"

#: package/contents/ui/code/tools.js:110
#, kde-format
msgid "On the Current Activity"
msgstr "Na actividá actual"

#: package/contents/ui/code/tools.js:124
#, kde-format
msgid "Show in Favorites"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:41
#, kde-format
msgid "Icon:"
msgstr "Iconu:"

#: package/contents/ui/ConfigGeneral.qml:47
#, kde-format
msgctxt "@action:button"
msgid "Change Application Launcher's icon"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:48
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"Current icon is %1. Click to open menu to change the current icon or reset "
"to the default icon."
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:52
#, kde-format
msgctxt "@info:tooltip"
msgid "Icon name is \"%1\""
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:85
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "Escoyer…"

#: package/contents/ui/ConfigGeneral.qml:87
#, kde-format
msgctxt "@info:whatsthis"
msgid "Choose an icon for Application Launcher"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:91
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Reset to default icon"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:97
#, kde-format
msgctxt "@action:inmenu"
msgid "Remove icon"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:108
#, kde-format
msgctxt "@label:textbox"
msgid "Text label:"
msgstr "Etiqueta de testu:"

#: package/contents/ui/ConfigGeneral.qml:110
#, kde-format
msgctxt "@info:placeholder"
msgid "Type here to add a text label"
msgstr "Escribi equí p'amestar una etiqueta de testu"

#: package/contents/ui/ConfigGeneral.qml:125
#, kde-format
msgctxt "@action:button"
msgid "Reset menu label"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:139
#, kde-format
msgctxt "@info"
msgid "A text label cannot be set when the Panel is vertical."
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:150
#, kde-format
msgctxt "General options"
msgid "General:"
msgstr "Xeneral:"

#: package/contents/ui/ConfigGeneral.qml:151
#, kde-format
msgid "Always sort applications alphabetically"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:156
#, kde-format
msgid "Use compact list item style"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:162
#, kde-format
msgctxt "@info:usagetip under a checkbox when Touch Mode is on"
msgid "Automatically disabled when in Touch Mode"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:171
#, kde-format
msgctxt "@action:button"
msgid "Configure Enabled Search Plugins…"
msgstr "Configurar los plugins de busca activaos…"

#: package/contents/ui/ConfigGeneral.qml:181
#, kde-format
msgid "Sidebar position:"
msgstr "Posición de la barra llateral:"

#: package/contents/ui/ConfigGeneral.qml:182
#: package/contents/ui/ConfigGeneral.qml:190
#, kde-format
msgid "Right"
msgstr "Derecha"

#: package/contents/ui/ConfigGeneral.qml:182
#: package/contents/ui/ConfigGeneral.qml:190
#, kde-format
msgid "Left"
msgstr "Esquierda"

#: package/contents/ui/ConfigGeneral.qml:198
#, kde-format
msgid "Show favorites:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:199
#, kde-format
msgctxt "Part of a sentence: 'Show favorites in a grid'"
msgid "In a grid"
msgstr "Nun rexáu"

#: package/contents/ui/ConfigGeneral.qml:207
#, kde-format
msgctxt "Part of a sentence: 'Show favorites in a list'"
msgid "In a list"
msgstr "Nuna llista"

#: package/contents/ui/ConfigGeneral.qml:215
#, kde-format
msgid "Show other applications:"
msgstr "Amosar otres aplicaciones:"

#: package/contents/ui/ConfigGeneral.qml:216
#, kde-format
msgctxt "Part of a sentence: 'Show other applications in a grid'"
msgid "In a grid"
msgstr "Nun rexáu"

#: package/contents/ui/ConfigGeneral.qml:224
#, kde-format
msgctxt "Part of a sentence: 'Show other applications in a list'"
msgid "In a list"
msgstr "Nuna llista"

#: package/contents/ui/ConfigGeneral.qml:236
#, kde-format
msgid "Show buttons for:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:237
#: package/contents/ui/LeaveButtons.qml:129
#, kde-format
msgid "Power"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:246
#, kde-format
msgid "Session"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:255
#, kde-format
msgid "Power and session"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:264
#, kde-format
msgid "Show action button captions"
msgstr ""

#: package/contents/ui/Footer.qml:97
#, kde-format
msgid "Applications"
msgstr "Aplicaciones"

#: package/contents/ui/Footer.qml:113
#, kde-format
msgid "Places"
msgstr "Llugares"

#: package/contents/ui/FullRepresentation.qml:153
#, kde-format
msgctxt "@info:status"
msgid "No matches"
msgstr ""

#: package/contents/ui/Header.qml:80
#, kde-format
msgid "Open user settings"
msgstr ""

#: package/contents/ui/Header.qml:259
#, kde-format
msgid "Keep Open"
msgstr ""

#: package/contents/ui/KickoffGridView.qml:92
#, kde-format
msgid "Grid with %1 rows, %2 columns"
msgstr ""

#: package/contents/ui/LeaveButtons.qml:129
#, kde-format
msgid "Leave"
msgstr ""

#: package/contents/ui/LeaveButtons.qml:129
#, kde-format
msgid "More"
msgstr "Más"

#: package/contents/ui/main.qml:311
#, kde-format
msgid "Edit Applications…"
msgstr ""

#: package/contents/ui/PlacesPage.qml:48
#, kde-format
msgctxt "category in Places sidebar"
msgid "Computer"
msgstr "Ordenador"

#: package/contents/ui/PlacesPage.qml:49
#, kde-format
msgctxt "category in Places sidebar"
msgid "History"
msgstr "Historial"

#: package/contents/ui/PlacesPage.qml:50
#, kde-format
msgctxt "category in Places sidebar"
msgid "Frequently Used"
msgstr "D'usu frecuente"
