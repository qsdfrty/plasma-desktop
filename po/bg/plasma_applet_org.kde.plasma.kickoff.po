# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Mincho Kondarev <mkondarev@yahoo.de>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-24 00:38+0000\n"
"PO-Revision-Date: 2023-09-15 18:22+0200\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: \n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.04.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Общи"

#: package/contents/ui/code/tools.js:32
#, kde-format
msgid "Remove from Favorites"
msgstr "Премахване от Предпочитани"

#: package/contents/ui/code/tools.js:36
#, kde-format
msgid "Add to Favorites"
msgstr "Добавяне в Предпочитани"

#: package/contents/ui/code/tools.js:60
#, kde-format
msgid "On All Activities"
msgstr "На всички дейности"

#: package/contents/ui/code/tools.js:110
#, kde-format
msgid "On the Current Activity"
msgstr "На текущата дейност"

#: package/contents/ui/code/tools.js:124
#, kde-format
msgid "Show in Favorites"
msgstr "Показване в Предпочитани"

#: package/contents/ui/ConfigGeneral.qml:41
#, kde-format
msgid "Icon:"
msgstr "Икона:"

#: package/contents/ui/ConfigGeneral.qml:47
#, kde-format
msgctxt "@action:button"
msgid "Change Application Launcher's icon"
msgstr "Промяна на иконата на стартера на приложения"

#: package/contents/ui/ConfigGeneral.qml:48
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"Current icon is %1. Click to open menu to change the current icon or reset "
"to the default icon."
msgstr ""
"Текущата икона е %1. Щракнете, за да отворите менюто за промяна на текущата "
"икона или за връщане на иконата по подразбиране."

#: package/contents/ui/ConfigGeneral.qml:52
#, kde-format
msgctxt "@info:tooltip"
msgid "Icon name is \"%1\""
msgstr "Името на иконата е \"%1\""

#: package/contents/ui/ConfigGeneral.qml:85
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "Избиране…"

#: package/contents/ui/ConfigGeneral.qml:87
#, kde-format
msgctxt "@info:whatsthis"
msgid "Choose an icon for Application Launcher"
msgstr "Избиране на икона за стартера на приложения"

#: package/contents/ui/ConfigGeneral.qml:91
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Reset to default icon"
msgstr "Възстановяване на стандартната икона"

#: package/contents/ui/ConfigGeneral.qml:97
#, kde-format
msgctxt "@action:inmenu"
msgid "Remove icon"
msgstr "Премахване на икона"

#: package/contents/ui/ConfigGeneral.qml:108
#, kde-format
msgctxt "@label:textbox"
msgid "Text label:"
msgstr "Надпис:"

#: package/contents/ui/ConfigGeneral.qml:110
#, kde-format
msgctxt "@info:placeholder"
msgid "Type here to add a text label"
msgstr "Добавете надпис"

#: package/contents/ui/ConfigGeneral.qml:125
#, kde-format
msgctxt "@action:button"
msgid "Reset menu label"
msgstr "Възстановяване на етикета на менюто"

#: package/contents/ui/ConfigGeneral.qml:139
#, kde-format
msgctxt "@info"
msgid "A text label cannot be set when the Panel is vertical."
msgstr "Не може да се зададе текстов етикет, когато панелът е вертикален."

#: package/contents/ui/ConfigGeneral.qml:150
#, kde-format
msgctxt "General options"
msgid "General:"
msgstr "Общи:"

#: package/contents/ui/ConfigGeneral.qml:151
#, kde-format
msgid "Always sort applications alphabetically"
msgstr "Сортиране на приложенията по азбучен ред винаги"

#: package/contents/ui/ConfigGeneral.qml:156
#, kde-format
msgid "Use compact list item style"
msgstr "Компактен стил на списък"

#: package/contents/ui/ConfigGeneral.qml:162
#, kde-format
msgctxt "@info:usagetip under a checkbox when Touch Mode is on"
msgid "Automatically disabled when in Touch Mode"
msgstr "Автоматично деактивиране в режим на таблет"

#: package/contents/ui/ConfigGeneral.qml:171
#, kde-format
msgctxt "@action:button"
msgid "Configure Enabled Search Plugins…"
msgstr "Конфигуриране на активираните приставки за търсене…"

#: package/contents/ui/ConfigGeneral.qml:181
#, kde-format
msgid "Sidebar position:"
msgstr "Позиция на плъзгача:"

#: package/contents/ui/ConfigGeneral.qml:182
#: package/contents/ui/ConfigGeneral.qml:190
#, kde-format
msgid "Right"
msgstr "Вдясно"

#: package/contents/ui/ConfigGeneral.qml:182
#: package/contents/ui/ConfigGeneral.qml:190
#, kde-format
msgid "Left"
msgstr "Вляво"

#: package/contents/ui/ConfigGeneral.qml:198
#, kde-format
msgid "Show favorites:"
msgstr "Показване на Предпочитани:"

#: package/contents/ui/ConfigGeneral.qml:199
#, kde-format
msgctxt "Part of a sentence: 'Show favorites in a grid'"
msgid "In a grid"
msgstr "Плочки"

#: package/contents/ui/ConfigGeneral.qml:207
#, kde-format
msgctxt "Part of a sentence: 'Show favorites in a list'"
msgid "In a list"
msgstr "Списък"

#: package/contents/ui/ConfigGeneral.qml:215
#, kde-format
msgid "Show other applications:"
msgstr "Показване на други приложения:"

#: package/contents/ui/ConfigGeneral.qml:216
#, kde-format
msgctxt "Part of a sentence: 'Show other applications in a grid'"
msgid "In a grid"
msgstr "Плочки"

#: package/contents/ui/ConfigGeneral.qml:224
#, kde-format
msgctxt "Part of a sentence: 'Show other applications in a list'"
msgid "In a list"
msgstr "Списък"

#: package/contents/ui/ConfigGeneral.qml:236
#, kde-format
msgid "Show buttons for:"
msgstr "Показване на бутони за:"

#: package/contents/ui/ConfigGeneral.qml:237
#: package/contents/ui/LeaveButtons.qml:129
#, kde-format
msgid "Power"
msgstr "Захранване"

#: package/contents/ui/ConfigGeneral.qml:246
#, kde-format
msgid "Session"
msgstr "Сесия"

#: package/contents/ui/ConfigGeneral.qml:255
#, kde-format
msgid "Power and session"
msgstr "Захранване и сесия"

#: package/contents/ui/ConfigGeneral.qml:264
#, kde-format
msgid "Show action button captions"
msgstr "Показване на надписите на бутоните за действие"

#: package/contents/ui/Footer.qml:97
#, kde-format
msgid "Applications"
msgstr "Приложения"

#: package/contents/ui/Footer.qml:113
#, kde-format
msgid "Places"
msgstr "Места"

#: package/contents/ui/FullRepresentation.qml:153
#, kde-format
msgctxt "@info:status"
msgid "No matches"
msgstr "Няма съвпадения"

#: package/contents/ui/Header.qml:80
#, kde-format
msgid "Open user settings"
msgstr "Потребителски настройки"

#: package/contents/ui/Header.qml:259
#, kde-format
msgid "Keep Open"
msgstr "Задържане отворен"

#: package/contents/ui/KickoffGridView.qml:92
#, kde-format
msgid "Grid with %1 rows, %2 columns"
msgstr "Плочки на %1 реда, %2 колони"

#: package/contents/ui/LeaveButtons.qml:129
#, kde-format
msgid "Leave"
msgstr "Напускане"

#: package/contents/ui/LeaveButtons.qml:129
#, kde-format
msgid "More"
msgstr "Повече"

#: package/contents/ui/main.qml:311
#, kde-format
msgid "Edit Applications…"
msgstr "Редактиране на приложения…"

#: package/contents/ui/PlacesPage.qml:48
#, kde-format
msgctxt "category in Places sidebar"
msgid "Computer"
msgstr "Компютър"

#: package/contents/ui/PlacesPage.qml:49
#, kde-format
msgctxt "category in Places sidebar"
msgid "History"
msgstr "История"

#: package/contents/ui/PlacesPage.qml:50
#, kde-format
msgctxt "category in Places sidebar"
msgid "Frequently Used"
msgstr "Често използвани"
