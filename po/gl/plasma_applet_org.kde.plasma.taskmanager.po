# translation of plasma_applet_tasks.po to Galician
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# mvillarino <mvillarino@users.sourceforge.net>, 2008.
# marce villarino <mvillarino@users.sourceforge.net>, 2009.
# marce villarino <mvillarino@gmail.com>, 2009.
# Marce Villarino <mvillarino@kde-espana.es>, 2011.
# Marce Villarino <mvillarino@kde-espana.es>, 2011, 2013.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015, 2016.
# SPDX-FileCopyrightText: 2023 Adrián Chaves (Gallaecio)
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_tasks\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:38+0000\n"
"PO-Revision-Date: 2023-11-16 18:45+0100\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.2\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "Appearance"
msgstr "Aparencia"

#: package/contents/config/config.qml:18
#, kde-format
msgid "Behavior"
msgstr "Comportamento"

#: package/contents/ui/AudioStream.qml:101
#, kde-format
msgctxt "@action:button"
msgid "Unmute"
msgstr "Activar o son"

#: package/contents/ui/AudioStream.qml:101
#, kde-format
msgctxt "@action:button"
msgid "Mute"
msgstr "Silenciar"

#: package/contents/ui/AudioStream.qml:102
#, kde-format
msgctxt "@info:tooltip %1 is the window title"
msgid "Unmute %1"
msgstr "Activar o son de %1."

#: package/contents/ui/AudioStream.qml:102
#, kde-format
msgctxt "@info:tooltip %1 is the window title"
msgid "Mute %1"
msgstr "Silenciar %1."

#: package/contents/ui/Badge.qml:54
#, kde-format
msgctxt "Invalid number of new messages, overlay, keep short"
msgid "—"
msgstr "—"

#: package/contents/ui/Badge.qml:56
#, kde-format
msgctxt "Over 9999 new messages, overlay, keep short"
msgid "9,999+"
msgstr "9.999+"

#: package/contents/ui/ConfigAppearance.qml:45
#, kde-format
msgid "General:"
msgstr "Xeral:"

#: package/contents/ui/ConfigAppearance.qml:46
#, kde-format
msgid "Show small window previews when hovering over Tasks"
msgstr "Amosar vistas previas de xanelas pequenas ao cubrir tarefas"

#: package/contents/ui/ConfigAppearance.qml:51
#, kde-format
msgid "Hide other windows when hovering over previews"
msgstr "Agochar outras xanelas ao cubrir vistas previas"

#: package/contents/ui/ConfigAppearance.qml:56
#, kde-format
msgid "Mark applications that play audio"
msgstr "Marcar as aplicacións que reproducen son"

#: package/contents/ui/ConfigAppearance.qml:64
#, kde-format
msgctxt "@option:check"
msgid "Fill free space on Panel"
msgstr "Encher o espazo libre no panel."

#: package/contents/ui/ConfigAppearance.qml:73
#, kde-format
msgctxt "@option: radio"
msgid "Use multi-column view:"
msgstr "Usar unha vista de varias columnas:"

#: package/contents/ui/ConfigAppearance.qml:73
#, kde-format
msgctxt "@option:radio"
msgid "Use multi-row view:"
msgstr "Usar unha vista de varias filas:"

#: package/contents/ui/ConfigAppearance.qml:79
#, kde-format
msgctxt "Never use multi-column view for Task Manager"
msgid "Never"
msgstr "Nunca"

#: package/contents/ui/ConfigAppearance.qml:89
#, kde-format
msgctxt "When to use multi-row view in Task Manager"
msgid "When Panel is low on space and thick enough"
msgstr "Cando o panel teña pouco espazo e sexa groso dabondo"

#: package/contents/ui/ConfigAppearance.qml:99
#, kde-format
msgctxt "When to use multi-row view in Task Manager"
msgid "Always when Panel is thick enough"
msgstr "Sempre que o panel sexa groso dabondo"

#: package/contents/ui/ConfigAppearance.qml:105
#, kde-format
msgctxt "@label:spinbox"
msgid "Maximum columns:"
msgstr "Columnas máximas:"

#: package/contents/ui/ConfigAppearance.qml:105
#, kde-format
msgctxt "@label:spinbox"
msgid "Maximum rows:"
msgstr "Filas máximas:"

#: package/contents/ui/ConfigAppearance.qml:115
#, kde-format
msgid "Spacing between icons:"
msgstr "Espazo entre iconas:"

#: package/contents/ui/ConfigAppearance.qml:119
#, kde-format
msgctxt "@item:inlistbox Icon spacing"
msgid "Small"
msgstr "Pequeno"

#: package/contents/ui/ConfigAppearance.qml:123
#, kde-format
msgctxt "@item:inlistbox Icon spacing"
msgid "Normal"
msgstr "Normal"

#: package/contents/ui/ConfigAppearance.qml:127
#, kde-format
msgctxt "@item:inlistbox Icon spacing"
msgid "Large"
msgstr "Grande"

#: package/contents/ui/ConfigAppearance.qml:151
#, kde-format
msgctxt "@info:usagetip under a set of radio buttons when Touch Mode is on"
msgid "Automatically set to Large when in Touch Mode"
msgstr "Faise grande automaticamente no modo táctil."

#: package/contents/ui/ConfigBehavior.qml:46
#, kde-format
msgid "Group:"
msgstr "Grupo:"

#: package/contents/ui/ConfigBehavior.qml:49
#, kde-format
msgid "Do not group"
msgstr "Non agrupar"

#: package/contents/ui/ConfigBehavior.qml:49
#, kde-format
msgid "By program name"
msgstr "Polo nome do programa"

#: package/contents/ui/ConfigBehavior.qml:54
#, kde-format
msgid "Clicking grouped task:"
msgstr "Premer tarefas agrupadas:"

#: package/contents/ui/ConfigBehavior.qml:61
#, kde-format
msgctxt "Completes the sentence 'Clicking grouped task cycles through tasks' "
msgid "Cycles through tasks"
msgstr "Circula polas tarefas"

#: package/contents/ui/ConfigBehavior.qml:62
#, kde-format
msgctxt ""
"Completes the sentence 'Clicking grouped task shows tooltip window "
"thumbnails' "
msgid "Shows small window previews"
msgstr "Amosa vistas previas de xanelas pequenas."

#: package/contents/ui/ConfigBehavior.qml:63
#, kde-format
msgctxt ""
"Completes the sentence 'Clicking grouped task shows windows side by side' "
msgid "Shows large window previews"
msgstr "Amosa vistas previas de xanelas grandes"

#: package/contents/ui/ConfigBehavior.qml:64
#, kde-format
msgctxt "Completes the sentence 'Clicking grouped task shows textual list' "
msgid "Shows textual list"
msgstr "Amosa unha lista de texto"

#: package/contents/ui/ConfigBehavior.qml:72
#, kde-format
msgid ""
"The compositor does not support displaying windows side by side, so a "
"textual list will be displayed instead."
msgstr ""
"O compositor non permite amosar xanelas unha a cada lado, así que se amosará "
"unha lista de texto no seu lugar."

#: package/contents/ui/ConfigBehavior.qml:82
#, kde-format
msgid "Combine into single button"
msgstr "Combinar nun botón"

#: package/contents/ui/ConfigBehavior.qml:89
#, kde-format
msgid "Group only when the Task Manager is full"
msgstr "Só agrupar cando o xestor de tarefas estea cheo"

#: package/contents/ui/ConfigBehavior.qml:100
#, kde-format
msgid "Sort:"
msgstr "Ordenar:"

#: package/contents/ui/ConfigBehavior.qml:103
#, kde-format
msgid "Do not sort"
msgstr "Non ordenar"

#: package/contents/ui/ConfigBehavior.qml:103
#, kde-format
msgid "Manually"
msgstr "Manualmente"

#: package/contents/ui/ConfigBehavior.qml:103
#, kde-format
msgid "Alphabetically"
msgstr "Alfabeticamente"

#: package/contents/ui/ConfigBehavior.qml:103
#, kde-format
msgid "By desktop"
msgstr "Por escritorio"

#: package/contents/ui/ConfigBehavior.qml:103
#, kde-format
msgid "By activity"
msgstr "Por actividade"

#: package/contents/ui/ConfigBehavior.qml:109
#, kde-format
msgid "Keep launchers separate"
msgstr "Manter os iniciadores separados"

#: package/contents/ui/ConfigBehavior.qml:120
#, kde-format
msgctxt "Part of a sentence: 'Clicking active task minimizes the task'"
msgid "Clicking active task:"
msgstr "Premer a tarefa activa:"

#: package/contents/ui/ConfigBehavior.qml:121
#, kde-format
msgctxt "Part of a sentence: 'Clicking active task minimizes the task'"
msgid "Minimizes the task"
msgstr "Minimiza a tarefa"

#: package/contents/ui/ConfigBehavior.qml:126
#, kde-format
msgid "Middle-clicking any task:"
msgstr "Clic central en calquera tarefa:"

#: package/contents/ui/ConfigBehavior.qml:130
#, kde-format
msgctxt "Part of a sentence: 'Middle-clicking any task does nothing'"
msgid "Does nothing"
msgstr "Non fai nada"

#: package/contents/ui/ConfigBehavior.qml:131
#, kde-format
msgctxt "Part of a sentence: 'Middle-clicking any task closes window or group'"
msgid "Closes window or group"
msgstr "Pecha a xanela ou o grupo"

#: package/contents/ui/ConfigBehavior.qml:132
#, kde-format
msgctxt "Part of a sentence: 'Middle-clicking any task opens a new window'"
msgid "Opens a new window"
msgstr "Abre unha nova xanela"

#: package/contents/ui/ConfigBehavior.qml:133
#, kde-format
msgctxt ""
"Part of a sentence: 'Middle-clicking any task minimizes/restores window or "
"group'"
msgid "Minimizes/Restores window or group"
msgstr "Minimiza ou restaura a xanela ou o grupo"

#: package/contents/ui/ConfigBehavior.qml:134
#, kde-format
msgctxt "Part of a sentence: 'Middle-clicking any task toggles grouping'"
msgid "Toggles grouping"
msgstr "Conmuta o agrupamento"

#: package/contents/ui/ConfigBehavior.qml:135
#, kde-format
msgctxt ""
"Part of a sentence: 'Middle-clicking any task brings it to the current "
"virtual desktop'"
msgid "Brings it to the current virtual desktop"
msgstr "Tráea ao escritorio virtual actual"

#: package/contents/ui/ConfigBehavior.qml:145
#, kde-format
msgctxt "Part of a sentence: 'Mouse wheel cycles through tasks'"
msgid "Mouse wheel:"
msgstr "Roda do rato:"

#: package/contents/ui/ConfigBehavior.qml:146
#, kde-format
msgctxt "Part of a sentence: 'Mouse wheel cycles through tasks'"
msgid "Cycles through tasks"
msgstr "Circula polas tarefas"

#: package/contents/ui/ConfigBehavior.qml:155
#, kde-format
msgid "Skip minimized tasks"
msgstr "Saltar as tarefas minimizadas"

#: package/contents/ui/ConfigBehavior.qml:166
#, kde-format
msgid "Show only tasks:"
msgstr "Só amosar as tarefas:"

#: package/contents/ui/ConfigBehavior.qml:167
#, kde-format
msgid "From current screen"
msgstr "Da pantalla actual."

#: package/contents/ui/ConfigBehavior.qml:172
#, kde-format
msgid "From current desktop"
msgstr "Do escritorio actual."

#: package/contents/ui/ConfigBehavior.qml:177
#, kde-format
msgid "From current activity"
msgstr "Da actividade actual."

#: package/contents/ui/ConfigBehavior.qml:182
#, kde-format
msgid "That are minimized"
msgstr "Minimizadas."

#: package/contents/ui/ConfigBehavior.qml:191
#, kde-format
msgid "When panel is hidden:"
msgstr "Cando se agocha o panel:"

#: package/contents/ui/ConfigBehavior.qml:192
#, kde-format
msgid "Unhide when a window wants attention"
msgstr "Amosar cando unha xanela queira atención."

#: package/contents/ui/ConfigBehavior.qml:204
#, kde-format
msgid "New tasks appear:"
msgstr "As novas tarefas aparecen:"

#: package/contents/ui/ConfigBehavior.qml:208
#, kde-format
msgid "On the bottom"
msgstr "Abaixo"

#: package/contents/ui/ConfigBehavior.qml:212
#: package/contents/ui/ConfigBehavior.qml:232
#, kde-format
msgid "To the right"
msgstr "Á dereita"

#: package/contents/ui/ConfigBehavior.qml:214
#: package/contents/ui/ConfigBehavior.qml:230
#, kde-format
msgid "To the left"
msgstr "Á esquerda"

#: package/contents/ui/ConfigBehavior.qml:226
#, kde-format
msgid "On the Top"
msgstr "Arriba"

#: package/contents/ui/ContextMenu.qml:100
#, kde-format
msgid "Places"
msgstr "Lugares"

#: package/contents/ui/ContextMenu.qml:106
#, kde-format
msgid "Recent Files"
msgstr "Ficheiros recentes"

#: package/contents/ui/ContextMenu.qml:113
#, kde-format
msgid "Actions"
msgstr "Accións"

#: package/contents/ui/ContextMenu.qml:174
#, kde-format
msgctxt "Play previous track"
msgid "Previous Track"
msgstr "Pista anterior"

#: package/contents/ui/ContextMenu.qml:188
#, kde-format
msgctxt "Pause playback"
msgid "Pause"
msgstr "Pór en pausa"

#: package/contents/ui/ContextMenu.qml:188
#, kde-format
msgctxt "Start playback"
msgid "Play"
msgstr "Reproducir"

#: package/contents/ui/ContextMenu.qml:206
#, kde-format
msgctxt "Play next track"
msgid "Next Track"
msgstr "Pista seguinte"

#: package/contents/ui/ContextMenu.qml:217
#, kde-format
msgctxt "Stop playback"
msgid "Stop"
msgstr "Deter"

#: package/contents/ui/ContextMenu.qml:237
#, kde-format
msgctxt "Quit media player app"
msgid "Quit"
msgstr "Saír"

#: package/contents/ui/ContextMenu.qml:252
#, kde-format
msgctxt "Open or bring to the front window of media player app"
msgid "Restore"
msgstr "Restaurar"

#: package/contents/ui/ContextMenu.qml:277
#, kde-format
msgid "Mute"
msgstr "Silenciar"

#: package/contents/ui/ContextMenu.qml:288
#, kde-format
msgid "Open New Window"
msgstr "Abrir unha nova xanela"

#: package/contents/ui/ContextMenu.qml:304
#, kde-format
msgid "Move to &Desktop"
msgstr "Mover ao &escritorio"

#: package/contents/ui/ContextMenu.qml:328
#, kde-format
msgid "Move &To Current Desktop"
msgstr "Mover ao escritorio &actual"

#: package/contents/ui/ContextMenu.qml:337
#, kde-format
msgid "&All Desktops"
msgstr "&Todos os escritorios"

#: package/contents/ui/ContextMenu.qml:351
#, kde-format
msgctxt "1 = number of desktop, 2 = desktop name"
msgid "&%1 %2"
msgstr "&%1 %2"

#: package/contents/ui/ContextMenu.qml:365
#, kde-format
msgid "&New Desktop"
msgstr "&Novo escritorio"

#: package/contents/ui/ContextMenu.qml:385
#, kde-format
msgid "Show in &Activities"
msgstr "Amosar nas &actividades"

#: package/contents/ui/ContextMenu.qml:409
#, kde-format
msgid "Add To Current Activity"
msgstr "Engadir á actividade actual"

#: package/contents/ui/ContextMenu.qml:419
#, kde-format
msgid "All Activities"
msgstr "Todas as actividades"

#: package/contents/ui/ContextMenu.qml:477
#, kde-format
msgid "Move to %1"
msgstr "Mover a %1"

#: package/contents/ui/ContextMenu.qml:505
#: package/contents/ui/ContextMenu.qml:522
#, kde-format
msgid "&Pin to Task Manager"
msgstr "&Fixar no xestor de tarefas"

#: package/contents/ui/ContextMenu.qml:575
#, kde-format
msgid "On All Activities"
msgstr "En todas as actividades"

#: package/contents/ui/ContextMenu.qml:581
#, kde-format
msgid "On The Current Activity"
msgstr "Na actividade actual"

#: package/contents/ui/ContextMenu.qml:606
#, kde-format
msgid "Unpin from Task Manager"
msgstr "Desprender do xestor de tarefas"

#: package/contents/ui/ContextMenu.qml:621
#, kde-format
msgid "More"
msgstr "Máis"

#: package/contents/ui/ContextMenu.qml:630
#, kde-format
msgid "&Move"
msgstr "&Mover"

#: package/contents/ui/ContextMenu.qml:639
#, kde-format
msgid "Re&size"
msgstr "Cambiar de &tamaño"

#: package/contents/ui/ContextMenu.qml:653
#, kde-format
msgid "Ma&ximize"
msgstr "Ma&ximizar"

#: package/contents/ui/ContextMenu.qml:667
#, kde-format
msgid "Mi&nimize"
msgstr "Mi&nimizar"

#: package/contents/ui/ContextMenu.qml:677
#, kde-format
msgid "Keep &Above Others"
msgstr "Manter enrib&a das outras"

#: package/contents/ui/ContextMenu.qml:687
#, kde-format
msgid "Keep &Below Others"
msgstr "Manter &baixo as outras"

#: package/contents/ui/ContextMenu.qml:699
#, kde-format
msgid "&Fullscreen"
msgstr "Pantalla &completa"

#: package/contents/ui/ContextMenu.qml:711
#, kde-format
msgid "&Shade"
msgstr "&Sombra"

#: package/contents/ui/ContextMenu.qml:727
#, kde-format
msgid "Allow this program to be grouped"
msgstr "Permitir agrupar este programa"

#: package/contents/ui/ContextMenu.qml:773
#, kde-format
msgctxt "@item:inmenu"
msgid "&Close All"
msgstr "Pe&chalo todo"

#: package/contents/ui/ContextMenu.qml:773
#, kde-format
msgid "&Close"
msgstr "&Pechar"

#: package/contents/ui/Task.qml:80
#, kde-format
msgctxt "@info:usagetip %1 application name"
msgid "Launch %1"
msgstr "Iniciar %1"

#: package/contents/ui/Task.qml:85
#, kde-format
msgctxt "@info:tooltip"
msgid "There is %1 new message."
msgid_plural "There are %1 new messages."
msgstr[0] "Hai %1 nova mensaxe."
msgstr[1] "Hai %1 novas mensaxes."

#: package/contents/ui/Task.qml:94
#, kde-format
msgctxt "@info:usagetip %1 task name"
msgid "Show Task tooltip for %1"
msgstr "Amosar a indicación emerxente de tarefa de %1."

#: package/contents/ui/Task.qml:100
#, kde-format
msgctxt "@info:usagetip %1 task name"
msgid "Show windows side by side for %1"
msgstr "Amosar as xanela unha a carón da outra para %1."

#: package/contents/ui/Task.qml:105
#, kde-format
msgctxt "@info:usagetip %1 task name"
msgid "Open textual list of windows for %1"
msgstr "Abrir unha lista de texto das xanelas para %1."

#: package/contents/ui/Task.qml:109
#, kde-format
msgid "Activate %1"
msgstr "Activar %1"

#: package/contents/ui/ToolTipInstance.qml:358
#, kde-format
msgctxt "button to unmute app"
msgid "Unmute %1"
msgstr "Activar o son de %1"

#: package/contents/ui/ToolTipInstance.qml:359
#, kde-format
msgctxt "button to mute app"
msgid "Mute %1"
msgstr "Silenciar %1"

#: package/contents/ui/ToolTipInstance.qml:382
#, kde-format
msgctxt "Accessibility data on volume slider"
msgid "Adjust volume for %1"
msgstr "Axustar o volume de %1"

#: package/contents/ui/ToolTipInstance.qml:398
#, kde-format
msgctxt "volume percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/ToolTipInstance.qml:402
#, kde-format
msgctxt "only used for sizing, should be widest possible string"
msgid "100%"
msgstr "100%"

#: package/contents/ui/ToolTipInstance.qml:426
#, kde-format
msgctxt "Comma-separated list of desktops"
msgid "On %1"
msgstr "En %1"

#: package/contents/ui/ToolTipInstance.qml:429
#, kde-format
msgctxt "Comma-separated list of desktops"
msgid "Pinned to all desktops"
msgstr "Fixada en todos os escritorios."

#: package/contents/ui/ToolTipInstance.qml:440
#, kde-format
msgctxt "Which virtual desktop a window is currently on"
msgid "Available on all activities"
msgstr "Dispoñíbel en todas as actividades"

#: package/contents/ui/ToolTipInstance.qml:462
#, kde-format
msgctxt "Activities a window is currently on (apart from the current one)"
msgid "Also available on %1"
msgstr "Tamén dispoñíbel en %1"

#: package/contents/ui/ToolTipInstance.qml:466
#, kde-format
msgctxt "Which activities a window is currently on"
msgid "Available on %1"
msgstr "Dispoñíbel en %1"

#: plugin/backend.cpp:312
#, kde-format
msgctxt "Show all user Places"
msgid "%1 more Place"
msgid_plural "%1 more Places"
msgstr[0] "%1 lugar máis"
msgstr[1] "%1 lugares máis"

#: plugin/backend.cpp:408
#, kde-format
msgid "Recent Downloads"
msgstr "Descargas recentes"

#: plugin/backend.cpp:410
#, kde-format
msgid "Recent Connections"
msgstr "Conexións recentes"

#: plugin/backend.cpp:412
#, kde-format
msgid "Recent Places"
msgstr "Lugares recentes"

#: plugin/backend.cpp:421
#, kde-format
msgctxt "@action:inmenu"
msgid "Forget Recent Downloads"
msgstr "Esquecer as descargas recentes"

#: plugin/backend.cpp:423
#, kde-format
msgctxt "@action:inmenu"
msgid "Forget Recent Connections"
msgstr "Esquecer as conexións recentes"

#: plugin/backend.cpp:425
#, kde-format
msgctxt "@action:inmenu"
msgid "Forget Recent Places"
msgstr "Esquecer os lugares recentes"

#: plugin/backend.cpp:427
#, kde-format
msgctxt "@action:inmenu"
msgid "Forget Recent Files"
msgstr "Esquecer os ficheiros recentes"

#~ msgid "Always arrange tasks in rows of as many columns"
#~ msgstr "Organizar as tarefas sempre en filas de tantas columnas."

#~ msgid "Always arrange tasks in columns of as many rows"
#~ msgstr "Organizar as tarefas sempre en columnas de tantas filas."

#~ msgid "Show tooltips"
#~ msgstr "Mostrar as axudas"

#~ msgid "Icon size:"
#~ msgstr "Tamaño de icona:"

#~ msgid "Start New Instance"
#~ msgstr "Iniciar unha nova instancia"

#~ msgid "More Actions"
#~ msgstr "Máis accións"

#, fuzzy
#~| msgid "Cycle through tasks with mouse wheel"
#~ msgid "Cycle through tasks"
#~ msgstr "Pasar polas tarefas coa roda do rato"

#~ msgid "On middle-click:"
#~ msgstr "Botón central:"

#~ msgctxt "The click action"
#~ msgid "None"
#~ msgstr "Nada."

#~ msgctxt "When clicking it would toggle grouping windows of a specific app"
#~ msgid "Group/Ungroup"
#~ msgstr "Agrupar ou desagrupar"

#~ msgid "Open groups in popups"
#~ msgstr "Abrir os grupos en xanelas emerxentes"

#~ msgid "Filter:"
#~ msgstr "Filtro:"

#~ msgid "Show only tasks from the current desktop"
#~ msgstr "Mostrar só as tarefas no escritorio actual"

#~ msgid "Show only tasks from the current activity"
#~ msgstr "Mostrar só as tarefas na actividade actual"

#~ msgid "Always arrange tasks in as many rows as columns"
#~ msgstr "Organizar as tarefas sempre en tantas filas como columnas"

#~ msgid "Always arrange tasks in as many columns as rows"
#~ msgstr "Organizar as tarefas sempre en tantas filas como columnas"

#, fuzzy
#~| msgid "Move To &Activity"
#~ msgid "Move to &Activity"
#~ msgstr "Mover á &actividade"

#~ msgid "Show progress and status information in task buttons"
#~ msgstr ""
#~ "Mostrar a información de progreso e de estado nos botóns de tarefas."

#~ msgctxt ""
#~ "Toggle action for showing a launcher button while the application is not "
#~ "running"
#~ msgid "&Pin"
#~ msgstr "&Fixar"

#~ msgid "&Pin"
#~ msgstr "&Fixar"

#~ msgctxt ""
#~ "Remove launcher button for application shown while it is not running"
#~ msgid "Unpin"
#~ msgstr "Soltar"

#~ msgid "Arrangement"
#~ msgstr "Disposición"

#~ msgid "Highlight windows"
#~ msgstr "Realzar as xanelas"

#~ msgid "Grouping and Sorting"
#~ msgstr "Agrupamento e ordenamento"

#~ msgid "Do Not Sort"
#~ msgstr "Non ordenar"

#, fuzzy
#~| msgctxt "@action:button Go to previous song"
#~| msgid "Previous"
#~ msgctxt "Go to previous song"
#~ msgid "Previous"
#~ msgstr "Anterior"

#, fuzzy
#~| msgctxt "@action:button Pause player"
#~| msgid "Pause"
#~ msgctxt "Pause player"
#~ msgid "Pause"
#~ msgstr "Pausar"

#~ msgctxt "Start player"
#~ msgid "Play"
#~ msgstr "Reproducir"

#, fuzzy
#~| msgctxt "@action:button Go to next song"
#~| msgid "Next"
#~ msgctxt "Go to next song"
#~ msgid "Next"
#~ msgstr "Seguinte"

#~ msgctxt "close this window"
#~ msgid "Close"
#~ msgstr "Pechar"

#~ msgid "Use launcher icons for running applications"
#~ msgstr "Usar iconas de iniciador para os programar en execución."

#~ msgid "Force row settings"
#~ msgstr "Forzar a configuración das filas"

#~ msgid "Collapse Group"
#~ msgstr "Recoller o grupo"

#~ msgid "Expand Group"
#~ msgstr "Expandir o grupo"

#~ msgid "Edit Group"
#~ msgstr "Editar o grupo"

#~ msgid "New Group Name: "
#~ msgstr "Nome do novo grupo:"

#~ msgid "Collapse Parent Group"
#~ msgstr "Recoller o grupo pai"
